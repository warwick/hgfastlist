/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HGFastList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class HGFastList extends View {

    private OnTouchListener onTouchListener;
    private static int contentWidth;
    private static int contentHeight;
    private static IHGFastList iHGFastList;
    private static HGFastListInfo hgFastListInfo;
    private static int firstPointerId;
    private static int firstPointerIdx;
    private static float firstTouchX;
    private static float firstTouchY;
    private static int touchPointerCount;
    private static float storedGestureAngleBaseOne;
    private static float currentGestureAngleBaseOne;
    private static float fullGestureAngleBaseOne;
    private static float touchAngle;
    private static Point viewCenterPoint;
    private static float onDownAngleObjectCumulativeBaseOne;
    private static float onUpAngleGestureBaseOne;
    private static float precisionRotation;
    private static float angleSnapBaseOne;
    private static float angleSnapNextBaseOne;
    private static float angleSnapProximity;
    private static boolean angleSnapHasSnapped;
    private static boolean suppressValidate;
    private static long quickTapTime;
    private static long gestureDownTime;
    private static float variableDialInner;
    private static float variableDialOuter;
    private static int imageRadius;
    private static RecyclerView recyclerView;
    private static float recyclerViewHeight;
    private static float verticalRange;
    private static float itemsPerPage;
    private static boolean useVariablePages;
    private static float itemCount;
    private static float columnCount;
    private static float pagesPerDial;
    private static int currentItemId;
    private static float maxRotation;
    private Drawable foregroundDrawable;
    /* Bottom of block field declarations */

    /* Top of block field declarations */
    public interface IHGFastList {

        void onDown(HGFastListInfo hgFastListInfo);
        void onPointerDown(HGFastListInfo hgFastListInfo);
        void onMove(HGFastListInfo hgFastListInfo);
        void onPointerUp(HGFastListInfo hgFastListInfo);
        void onUp(HGFastListInfo hgFastListInfo);

    }


    final public class HGFastListInfo {

        private boolean quickTap;
        private boolean rotateSnapped;
        private boolean hasItemIdChanged;
        private boolean isMaximumReached;
        private boolean isMinimumReached;
        private int itemId;
        private int scrollDirection;

        public HGFastListInfo() {

            this.quickTap = false;
            this.rotateSnapped = false;
            this.hasItemIdChanged = false;
            this.isMaximumReached = false;
            this.isMinimumReached = false;
            this.itemId = -1;
            this.scrollDirection = 0;

        }

        /* Accessors */
        public boolean getQuickTap() {return this.quickTap;}
        public boolean getRotateSnapped() {return this.rotateSnapped;}
        public boolean getHasItemIdChanged() {return this.hasItemIdChanged;}
        public boolean getIsMaximumReached() {return this.isMaximumReached;}
        public boolean getIsMinimumReached() {return this.isMinimumReached;}
        public int getItemId() {return this.itemId;}
        public int getScrollDirection() {return this.scrollDirection;}

        /* Mutators */
        public void setQuickTap(boolean quickTap) {this.quickTap = quickTap;}
        public void setRotateSnapped(boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
        public void setHasItemIdChanged(boolean hasItemIdChanged) {this.hasItemIdChanged = hasItemIdChanged;}
        public void setIsMaximumReached(boolean isMaximumReached) {this.isMaximumReached = isMaximumReached;}
        public void setIsMinimumReached(boolean isMinimumReached) {this.isMinimumReached = isMinimumReached;}
        public void setItemId(int itemId) {this.itemId = itemId;}
        public void setScrollDirection(int scrollDirection) {this.scrollDirection = scrollDirection;}

    }//End final public class HGFastListInfo


    public HGFastList(Context context) {
        super(context);

        setOnTouchListener(getOnTouchListerField());
        init(null, 0);
        setFields();

    }//End public HGFastList(Context context)


    public HGFastList(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOnTouchListener(getOnTouchListerField());
        init(attrs, 0);
        setFields();

    }//End public HGFastList(Context context, AttributeSet attrs)


    private void setFields() {

        this.onTouchListener = null;
        this.contentWidth = 0;
        this.contentHeight = 0;
        this.iHGFastList = null;
        this.hgFastListInfo = new HGFastListInfo();
        this.firstPointerId = 0;
        this.firstPointerIdx = 0;
        this.firstTouchX = 0f;
        this.firstTouchY = 0f;
        this.touchPointerCount = 0;
        this.storedGestureAngleBaseOne = 0f;
        this.currentGestureAngleBaseOne = 0f;
        this.fullGestureAngleBaseOne = 0f;
        this.touchAngle = 0f;
        this.viewCenterPoint = null;
        this.onDownAngleObjectCumulativeBaseOne = 0f;
        this.onUpAngleGestureBaseOne = 0f;
        this.precisionRotation = 0f;
        this.angleSnapBaseOne = 0f;
        this.angleSnapNextBaseOne = 0f;
        this.angleSnapProximity = 0f;
        this.angleSnapHasSnapped = false;
        this.suppressValidate = false;
        this.quickTapTime = 100;
        this.gestureDownTime = 0;
        this.variableDialInner = 1.0f;
        this.variableDialOuter = 1.0f;
        this.imageRadius = 0;
        this.recyclerView = null;
        this.verticalRange = 0f;
        this.itemsPerPage = 0f;
        this.itemCount = 0f;
        this.columnCount = 1;
        this.pagesPerDial = 1f;
        currentItemId = 0;
        this.maxRotation = 0f;

    }//End private void setFields()


    public void registerCallback(IHGFastList iHGFastList) {

        HGFastList.iHGFastList = iHGFastList;

    }


    public HGFastListInfo getHgFastList() {

        return hgFastListInfo;

    }


    private void init(AttributeSet attrs, int defStyle) {

        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGFastList, defStyle, 0);

        if(a.hasValue(R.styleable.HGFastList_hg_drawable)) {

            foregroundDrawable = a.getDrawable(R.styleable.HGFastList_hg_drawable);
            foregroundDrawable.setCallback(this);

        }

        a.recycle();

    }//End private void init(AttributeSet attrs, int defStyle)


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(viewCenterPoint == null) {

            contentWidth = getWidth();
            contentHeight = getHeight();

            if(contentWidth >= contentHeight) {

                imageRadius = (int) ((float) contentHeight / 2f);

            }
            else if(contentWidth < contentHeight) {

                imageRadius = (int) ((float) contentWidth / 2f);

            }//End if(contentWidth >= contentHeight)

            viewCenterPoint = new Point(contentWidth / 2, contentHeight / 2);
            foregroundDrawable.setBounds(0, 0, contentWidth, contentHeight);

        }
        else if(viewCenterPoint != null) {

            if(angleSnapBaseOne == 0) {

                canvas.rotate((fullGestureAngleBaseOne * 360), viewCenterPoint.x, viewCenterPoint.y);

            }
            else if(angleSnapBaseOne != 0) {

                canvas.rotate((angleSnapNextBaseOne * 360), viewCenterPoint.x, viewCenterPoint.y);

            }//End if(rapidDial != 0)

        }//End if(viewCenterPoint == null)

        if(foregroundDrawable != null) {

            foregroundDrawable.draw(canvas);

        }

    }//End protected void onDraw(Canvas canvas)


    public Drawable getForegroundDrawable() {

        return foregroundDrawable;

    }//End public Drawable getForegroundDrawable()


    /* Top of block touch methods */
    private void setDownTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount == 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                }

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setDownTouch(final MotionEvent event)


    private void setMoveTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount < 2) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                }

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setMoveTouch(final MotionEvent event)


    private void setUpTouch(final MotionEvent event) {

        try {

            try {

                touchPointerCount = event.getPointerCount();

                if(touchPointerCount == 1) {

                    firstPointerId = event.getPointerId(0);
                    firstPointerIdx = event.findPointerIndex(firstPointerId);
                    firstTouchX = event.getX(firstPointerIdx);
                    firstTouchY = event.getY(firstPointerIdx);

                }

            }
            catch(IndexOutOfBoundsException e) {

                return;

            }

        }
        catch(IllegalArgumentException e) {

            return;

        }

    }//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


    /* Top of block rotate functions */
    private HGFastListInfo doDownDial() {

        if(touchPointerCount == 1) {

            touchAngle = getAngleFromPoint(viewCenterPoint, new Point((int) firstTouchX, (int) firstTouchY));

        }

        onDownAngleObjectCumulativeBaseOne = touchAngle - onUpAngleGestureBaseOne;
        setReturnType();

        return hgFastListInfo;

    }//End private HGResult doDownDial()


    private HGFastListInfo doMoveDial() {

        if(touchPointerCount == 1) {

            touchAngle = getAngleFromPoint(viewCenterPoint, new Point((int) firstTouchX, (int) firstTouchY));

        }

        setReturnType();
        return hgFastListInfo;

    }//End private HGFastListInfo doMoveDial()


    private HGFastListInfo doUpDial() {

        if(touchPointerCount == 1) {

            touchAngle = getAngleFromPoint(viewCenterPoint, new Point((int) firstTouchX, (int) firstTouchY));

        }
        else if(touchPointerCount == 0) {//Condition for programatic quickTap

            return hgFastListInfo;

        }

        onUpAngleGestureBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - touchAngle)) % 1;
        setReturnType();

        return hgFastListInfo;

    }//End private HGResult doUpDial()


    private void setReturnType() {

        setVariableDialAngleAndReturnDirection();

        if(angleSnapBaseOne != 0) {

            checkNextAngleSnapBaseOne();

        }

        if(suppressValidate == false) {invalidate();}

    }//End private void setReturnType()
    /* Bottom of block rotate functions */


    /* Top of block main functions */
    private int setVariableDialAngleAndReturnDirection() {

        final int[] returnValue = new int[1];
        currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - touchAngle));
        final float angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne) % 1;

        if(useVariablePages == true) {

			precisionRotation = getVariablePrecision();

        }
        else {

			precisionRotation = pagesPerDial;

        }

        //Detect direction
        if(!(Math.abs(angleDifference) > 0.5f)) {

            if(angleDifference > 0) {

                returnValue[0] = -1;
                fullGestureAngleBaseOne -= ((angleDifference * precisionRotation));

            }
            else if(angleDifference < 0) {

                returnValue[0] = 1;
                fullGestureAngleBaseOne += -(angleDifference * precisionRotation);

            }//End if(angleDifference > 0)

        }//End if(!(Math.abs(angleDifference) > 0.5f))

        storedGestureAngleBaseOne = currentGestureAngleBaseOne;

        if(fullGestureAngleBaseOne < 0f) {

            fullGestureAngleBaseOne = 0;
            hgFastListInfo.setIsMinimumReached(true);

        }
        else if(fullGestureAngleBaseOne > maxRotation) {

            fullGestureAngleBaseOne = maxRotation;
            hgFastListInfo.setIsMaximumReached(true);

        }
        else {

            hgFastListInfo.setIsMinimumReached(false);
            hgFastListInfo.setIsMaximumReached(false);

        }//End if(fullGestureAngleBaseOne < 0f)

        hgFastListInfo.setScrollDirection(returnValue[0]);
        doDialScroll((Math.abs(angleDifference * recyclerViewHeight) * returnValue[0]));

        return returnValue[0];

    }//End private int setVariableDialAngleAndReturnDirection()


    public void setScrollableView(final View scrollableView) {

        post(new Runnable() {
            @Override
            public void run() {

                recyclerView = (RecyclerView) scrollableView;
                columnCount = recyclerView.getLayoutManager().getColumnCountForAccessibility(null, null);
                recyclerViewHeight = (float) recyclerView.getHeight();
                verticalRange = (float) recyclerView.computeVerticalScrollRange();
                itemCount = (float) recyclerView.getAdapter().getItemCount();
                itemsPerPage = itemCount / (verticalRange / recyclerViewHeight);
                maxRotation = (verticalRange / recyclerViewHeight) - (recyclerViewHeight / (verticalRange + recyclerViewHeight));

            }
        });

    }//End public void setScrollableView(final View scrollableView)


    private void doDialScroll(float scrollBy) {

        final int itemId = (int) (itemsPerPage * fullGestureAngleBaseOne);
        recyclerView.scrollBy(0, (int) (scrollBy * precisionRotation));
        hgFastListInfo.setItemId(itemId);

        if(hgFastListInfo.getItemId() != currentItemId) {

            hgFastListInfo.setHasItemIdChanged(true);

        }
        else if(hgFastListInfo.getItemId() == currentItemId) {

            hgFastListInfo.setHasItemIdChanged(false);

        }//End if(hgFastListInfo.getItemId() != currentItemId)

        currentItemId = hgFastListInfo.getItemId();

    }//End private void doDialScroll(float scrollBy)


    private float getVariablePrecision() {

        final float distanceFromCenter = getTwoFingerDistance(viewCenterPoint.x, viewCenterPoint.y, firstTouchX, firstTouchY);

        if(distanceFromCenter <= imageRadius) {

            precisionRotation = (variableDialInner - (((distanceFromCenter / imageRadius) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;

        }

        return precisionRotation;

    }//End private float getVariablePrecision()


    private void checkNextAngleSnapBaseOne() {

        final float tempAngleSnap = Math.round(fullGestureAngleBaseOne / angleSnapBaseOne);
        angleSnapNextBaseOne = tempAngleSnap * angleSnapBaseOne;

        if(Math.abs(fullGestureAngleBaseOne - angleSnapNextBaseOne) < angleSnapProximity) {

            angleSnapHasSnapped = true;
            hgFastListInfo.setRotateSnapped(true);

        }
        else {

            angleSnapNextBaseOne = fullGestureAngleBaseOne;
            angleSnapHasSnapped = false;
            hgFastListInfo.setRotateSnapped(false);

        }

    }//End private void checkNextAngleSnapBaseOne()
    /* Bottom of block main functions */


    /* Top of block Accessors */
    public long getQuickTapTime() {return this.quickTapTime;}
    public float getAngleSnapBaseOne() {return this.angleSnapBaseOne;}
    public float getAngleSnapProximity() {return this.angleSnapProximity;}
    public OnTouchListener retrieveLocalOnTouchListener() {return onTouchListener;}
    public boolean hasAngleSnapped() {return angleSnapHasSnapped;}
    public float getVariableDialInner() {return this.variableDialInner;}
    public float getVariableDialOuter() {return this.variableDialOuter;}
    public float getItemsPerPage() {return this.itemsPerPage;}
    public boolean getUseVariablePages() {return this.useVariablePages;}
    public float getPrecisionRotation() {return this.precisionRotation;}
    public boolean getSuppressValidate() {return this.suppressValidate;}
    public View getScrollableView() {return HGFastList.recyclerView;}
    public float getDialAngle() {return this.fullGestureAngleBaseOne;}
    /* Bottom of block Accessors */


    /* Top of block Mutators */
    public void setQuickTapTime(long quickTapTime) {this.quickTapTime = quickTapTime;}
    public void setSuppressValidate(boolean suppressValidate) {this.suppressValidate = suppressValidate;}
    public void setDialAngle(float fullGestureAngleBaseOne) {this.fullGestureAngleBaseOne = fullGestureAngleBaseOne;}


    public void setAngleSnapBaseOne(final float angleSnapBaseOne, final float angleSnapProximity) {

        this.angleSnapBaseOne = (angleSnapBaseOne) % 1;
        this.angleSnapProximity = (angleSnapProximity) % 1;

        if(angleSnapProximity > angleSnapBaseOne / 2f) {this.angleSnapProximity = angleSnapBaseOne / 2;}

    }//End public void setAngleSnapBaseOne(final float angleSnapBaseOne, final float angleSnapProximity)


    public void setVariableDial(float variableDialInner, float variableDialOuter, float pagesPerDial) {

        this.variableDialInner = variableDialInner;
        this.variableDialOuter = variableDialOuter;
        this.pagesPerDial = pagesPerDial;

    }//End public void setVariableDial(float variableDialInner, float variableDialOuter, float pagesPerDial)


    public void setUseVariablePages(boolean useVariablePages) {

        this.useVariablePages = useVariablePages;

    }
    /* Bottom of block Mutators */


    /* Top of block convenience methods */
    public void performQuickTap() {

        hgFastListInfo.setQuickTap(true);

        post(new Runnable() {
            @Override
            public void run() {

                iHGFastList.onUp(doUpDial());
                hgFastListInfo.setQuickTap(false);

            }
        });

    }//End public void performQuickTap()


    /* Top of block geometry functions */
    private float getAngleFromPoint(final Point centerPoint, final Point touchPoint) {

        float returnVal = 0;

        //+0 - 0.5
        if(touchPoint.x > centerPoint.x) {

            returnVal = (float) (Math.atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI);

        }
        //+0.5
        else if(touchPoint.x < centerPoint.x) {

            returnVal = (float)  (1 - (Math.atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI));

        }//End if(touchPoint.x > centerPoint.x)

        return returnVal;

    }//End private float getAngleFromPoint(final Point centerPoint, final Point touchPoint)


    private static float getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY) {

        float pinchDistanceX = 0;
        float pinchDistanceY = 0;

        if(firstTouchX > secondTouchX) {

            pinchDistanceX = Math.abs(secondTouchX - firstTouchX);

        }
        else if(firstTouchX < secondTouchX) {

            pinchDistanceX = Math.abs(firstTouchX - secondTouchX);

        }//End if(firstTouchX > secondTouchX)

        if(firstTouchY > secondTouchY) {

            pinchDistanceY = Math.abs(secondTouchY - firstTouchY);

        }
        else if(firstTouchY < secondTouchY) {

            pinchDistanceY = Math.abs(firstTouchY - secondTouchY);

        }//End if(firstTouchY > secondTouchY)

        if(pinchDistanceX == 0 && pinchDistanceY == 0) {

            return  0;

        }
        else {

            pinchDistanceX = (pinchDistanceX * pinchDistanceX);
            pinchDistanceY = (pinchDistanceY * pinchDistanceY);
            return (float) Math.abs(Math.sqrt(pinchDistanceX + pinchDistanceY));

        }//End if(pinchDistanceX == 0 && pinchDistanceY == 0)

    }//End private static float getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY)
    /* Bottom of block geometry functions */


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        sendTouchEvent(this, event);

        return true;

    }


    private OnTouchListener getOnTouchListerField() {

        onTouchListener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                hgTouchEvent(v, event);

                return true;

            }

        };

        return onTouchListener;

    }//End private OnTouchListener getOnTouchListerField()


    public void sendTouchEvent(View v, MotionEvent event) {

        hgTouchEvent(v, event);

    }


    private void hgTouchEvent(View v, MotionEvent event) {

        final int action = event.getAction() & MotionEvent.ACTION_MASK;

        //Top of block used for quick tap
        if(event.getAction() == MotionEvent.ACTION_DOWN) {

            hgFastListInfo.setQuickTap(false);//Used for quick tap
            gestureDownTime = System.currentTimeMillis();

        }
        else if(event.getAction() == MotionEvent.ACTION_UP) {

            //Used for quick tap
            if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

                hgFastListInfo.setQuickTap(true);
                iHGFastList.onUp(doUpDial());
                hgFastListInfo.setQuickTap(false);
                touchPointerCount = 0;

                return;

            }
            else {

                hgFastListInfo.setQuickTap(false);

            }//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

        }//End if(event.getAction() == MotionEvent.ACTION_DOWN)
        //Bottom of block used for quick tap

        switch(action) {

            case MotionEvent.ACTION_DOWN: {

                setDownTouch(event);
                iHGFastList.onDown(doDownDial());

                break;

            }
            case MotionEvent.ACTION_POINTER_DOWN: {

                setDownTouch(event);
                iHGFastList.onPointerDown(doDownDial());

                break;

            }
            case MotionEvent.ACTION_MOVE: {

                setMoveTouch(event);
                iHGFastList.onMove(doMoveDial());

                break;

            }
            case MotionEvent.ACTION_POINTER_UP: {

                setUpTouch(event);
                iHGFastList.onPointerUp(doUpDial());

                break;

            }
            case MotionEvent.ACTION_UP: {

                setUpTouch(event);
                iHGFastList.onUp(doUpDial());
                touchPointerCount = 0;

                break;

            }
            default:

                break;

        }//End switch(action)

    }//End private void hgTouchEvent(View v, MotionEvent event)

}