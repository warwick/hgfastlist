package com.WarwickWestonWright.HGFastListDemo;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;

public class HGSmoothScroller extends LinearLayoutManager  {

    private static final float MILLISECONDS_PER_INCH = 150f;

    public HGSmoothScroller(Context context) {
        super(context, VERTICAL, false);
    }

    public HGSmoothScroller(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {

        RecyclerView.SmoothScroller smoothScroller = new TopSnappedSmoothScroller(recyclerView.getContext());
        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);

    }

    private class TopSnappedSmoothScroller extends LinearSmoothScroller {
        public TopSnappedSmoothScroller(Context context) {
            super(context);

        }

        public PointF computeScrollVectorForPosition(int targetPosition) {

            return HGSmoothScroller.this.computeScrollVectorForPosition(targetPosition);

        }

        //This returns the milliseconds it takes to
        //scroll one pixel.
        @Override
        protected float calculateSpeedPerPixel
        (DisplayMetrics displayMetrics) {

            return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;

        }

    }

}