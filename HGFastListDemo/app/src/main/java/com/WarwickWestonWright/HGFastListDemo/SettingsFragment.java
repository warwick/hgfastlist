/*
Though this is an open source repository, only the files contained within the library are protected under an open source license and not the files that use the library.
The code in this file is NOT protected by any license and is free source. Feel free to use modify and or distribute with no obligation to the developer. If you wish to contact the developer you can do so at: warwickwestonwright@gmail.com
*/

package com.WarwickWestonWright.HGFastListDemo;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsFragment extends DialogFragment {

    private DialogInterface.OnClickListener onClickListener;
    private View rootView;

    //Declare Widgets
    private Button btnClose;
    private EditText txtPagesPerDial;
    private EditText txtInnerPrecision;
    private EditText txtOuterPrecision;
    private EditText txtAngleSnap;
    private EditText txtAngleSnapProximity;
    private CheckBox chkUseAngleSnap;
    private CheckBox chkUseVariablePages;
    private CheckBox chkSuppressInvalidate;
    private EditText txtListLength;

    private static SharedPreferences sharedPreferences;

    public SettingsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.settings_fragment, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //Instantiate Widgets
        btnClose = (Button) rootView.findViewById(R.id.btnClose);
        txtPagesPerDial = (EditText) rootView.findViewById(R.id.txtPagesPerDial);
        txtInnerPrecision = (EditText) rootView.findViewById(R.id.txtInnerPrecision);
        txtOuterPrecision = (EditText) rootView.findViewById(R.id.txtOuterPrecision);
        txtAngleSnap = (EditText) rootView.findViewById(R.id.txtAngleSnap);
        txtAngleSnapProximity = (EditText) rootView.findViewById(R.id.txtAngleSnapProximity);
        chkUseAngleSnap = (CheckBox) rootView.findViewById(R.id.chkUseAngleSnap);
        chkUseVariablePages = (CheckBox) rootView.findViewById(R.id.chkUseVariablePages);
        chkSuppressInvalidate = (CheckBox) rootView.findViewById(R.id.chkSuppressInvalidate);
        txtListLength = (EditText) rootView.findViewById(R.id.txtListLength);

        //Pick up sharedPrefs vals
        txtPagesPerDial.setText(Float.toString(sharedPreferences.getFloat("txtPagesPerDial", 8f)));
        txtInnerPrecision.setText(Float.toString(sharedPreferences.getFloat("txtInnerPrecision", 4.5f)));
        txtOuterPrecision.setText(Float.toString(sharedPreferences.getFloat("txtOuterPrecision", 0.8f)));
        txtAngleSnap.setText(Float.toString(sharedPreferences.getFloat("txtAngleSnap", 0.125f)));
        txtAngleSnapProximity.setText(Float.toString(sharedPreferences.getFloat("txtAngleSnapProximity", 0.03125f)));
        chkUseAngleSnap.setChecked(sharedPreferences.getBoolean("chkUseAngleSnap", false));
        chkUseVariablePages.setChecked(sharedPreferences.getBoolean("chkUseVariablePages", true));
        chkSuppressInvalidate.setChecked(sharedPreferences.getBoolean("chkSuppressInvalidate", false));
        txtListLength.setText(Integer.toString(sharedPreferences.getInt("txtListLength", 1000)));

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Float.parseFloat(txtPagesPerDial.getText().toString()) < 0) {

                    Toast.makeText(getActivity(), "Pages per dial must be equal to or greater than zero!", Toast.LENGTH_LONG).show();

                }
                else if(Float.parseFloat(txtPagesPerDial.getText().toString()) >= 0) {

                    sharedPreferences.edit().putFloat("txtPagesPerDial", Float.parseFloat(txtPagesPerDial.getText().toString())).commit();
                    sharedPreferences.edit().putFloat("txtInnerPrecision", Float.parseFloat(txtInnerPrecision.getText().toString())).commit();
                    sharedPreferences.edit().putFloat("txtOuterPrecision", Float.parseFloat(txtOuterPrecision.getText().toString())).commit();
                    sharedPreferences.edit().putFloat("txtAngleSnap", Float.parseFloat(txtAngleSnap.getText().toString())).commit();
                    sharedPreferences.edit().putFloat("txtAngleSnapProximity", Float.parseFloat(txtAngleSnapProximity.getText().toString())).commit();
                    sharedPreferences.edit().putBoolean("chkUseAngleSnap", chkUseAngleSnap.isChecked()).commit();
                    sharedPreferences.edit().putBoolean("chkUseVariablePages", chkUseVariablePages.isChecked()).commit();
                    sharedPreferences.edit().putBoolean("chkSuppressInvalidate", chkSuppressInvalidate.isChecked()).commit();

                    if(Integer.parseInt(txtListLength.getText().toString().trim()) != sharedPreferences.getInt("txtListLength", 1000)) {

                        sharedPreferences.edit().putInt("txtListLength", Integer.parseInt(txtListLength.getText().toString())).commit();
                        onClickListener.onClick(getDialog(), sharedPreferences.getInt("txtListLength", 1000));

                    }
                    else {

                        onClickListener.onClick(getDialog(), 0);

                    }

                }//End if(Float.parseFloat(txtPagesPerDial.getText().toString()) < 0)

            }
        });

        return rootView;

    }//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof DialogInterface.OnClickListener) {

            onClickListener = (DialogInterface.OnClickListener) context;

        }
        else {

            throw new RuntimeException(context.toString() + " must implement DialogInterface.OnClickListener");

        }

    }


    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();

        if(dialog != null) {

            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);

        }

    }


    @Override
    public void onDetach() {
        super.onDetach();

        onClickListener = null;

    }

}