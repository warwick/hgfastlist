package com.WarwickWestonWright.HGFastListDemo.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

    public static List<DummyItem> ITEMS = null;
    public static Map<String, DummyItem> ITEM_MAP = null;
    private static int count;

    public static void setCount(int count) {

        DummyContent.count = count;
        ITEMS = new ArrayList<DummyItem>();
        ITEM_MAP = new HashMap<String, DummyItem>();
        setupItems();

    }

    private static void setupItems() {

        for(int i = 0; i < count; i++) {

            addItem(createDummyItem(i));

        }

    }


    private static void addItem(DummyItem item) {

        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);

    }

    private static DummyItem createDummyItem(int position) {

        return new DummyItem(String.valueOf(position), "Item " + position, makeDetails(position));

    }


    private static String makeDetails(int position) {

        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);

        for(int i = 0; i < 5; i++) {

            builder.append("\nMore details information here.");

        }

        /* What was Google Thinking here
        for(int i = 0; i < position; i++) {

            builder.append("\nMore details information here.");

        }
        */

        return builder.toString();

    }


    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;

        public DummyItem(String id, String content, String details) {
            this.id = id;
            this.content = content;
            this.details = details;
        }

        @Override
        public String toString() {

            return content;

        }

    }

}