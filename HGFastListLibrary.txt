HG Fast List Library (Documentation).
Section 1 About the HGFastList library.
The HGFastList library was designed to address a long standing problem with navigating large lists
on mobile devices.

How it works:
The library implements a dial (preferably transparent) on top of a list. Rotating the dial moves the list up and down. It is possible to manipulate how fast the list moves, depending upon how close the gesture is to the centre of the dial. To vary how fast the list moves in response to the gesture you will need to call: setUseVariablePages(boolean useVariablePages)
History:
The code in this library was based on the HGGLDial, HGDial and HacerGestoV2 gesture libraries. The
HGGLDial and HGDial libraries are custom dial widgets for Android, while the HacerGestoV2 library
is a generic Gesture library (also for Android). All these libraries have advanced rotation functions
and behaviours.

Before this library existed the best way of navigating large lists was the iOS list control that had an
�out of the box� behaviour, that detected how close a touch was to the bottom or the top of a list
control, which varied the speed of the list movement.

Section 2 Usage:
Implementing this library is done in 4 simple steps.
Step 1. Instantiation Java Usage
HGFastList hgFastList = (HGFastList) findViewById(R.id.hgFastListView);

Step 2. Register the callback:
hgFastList.registerCallback(new HGFastList.IHGFastList() {
	@Override
	public void onDown(HGFastList.HGFastListInfo hgFastListInfo) {}
	@Override
	public void onPointerDown(HGFastList.HGFastListInfo hgFastListInfo) {}
	@Override
	public void onMove(HGFastList.HGFastListInfo hgFastListInfo) {}
	@Override
	public void onPointerUp(HGFastList.HGFastListInfo hgFastListInfo) {}
	@Override
	public void onUp(HGFastList.HGFastListInfo hgFastListInfo) {}
});
Or implement HGFastList.IHGFastList in your Activity or Fragment and call
hgFastList.registerCallback(this)
Step 3. Then call: hgFastList.setScrollableView(recyclerView);

Step 4. Setup the XML Layout
XML Layout Usage:
<FrameLayout
android:layout_width="wrap_content"
android:layout_height="wrap_content"
android:layout_centerHorizontal="true"
android:layout_above="@+id/lLayoutButtonContainer">
<com.WarwickWestonWright.HGFastList.HGFastList
android:id="@+id/hgFastListView"
android:layout_width="wrap_content"
android:layout_height="wrap_content"
xmlns:app="http://schemas.android.com/apk/res-auto"
app:hg_drawable="@drawable/hg_fast_list">
</FrameLayout>

Section 3 HGFastListInfo Class Description:
The HGFastListInfo is the dynamic return type returned from the libraries callback. This callback hooks into the gesture touch events.
This Class has four fields with classic accessors and mutators:
boolean quickTap;
boolean rotateSnapped;
hasItemIdChanged;
isMaximumReached;
isMinimumReached;
int itemId;
int scrollDirection;

/* Accessors */
boolean getQuickTap()
boolean getRotateSnapped()
boolean getHasItemIdChanged() {return this.hasItemIdChanged;}
boolean getIsMaximumReached() {return this.isMaximumReached;}
boolean getIsMinimumReached() {return this.isMinimumReached;}
int getItemId()
int getScrollDirection()

/* Mutators */
void setQuickTap(boolean quickTap)
void setRotateSnapped(boolean rotateSnapped)
void setHasItemIdChanged(boolean hasItemIdChanged)
void setIsMaximumReached(boolean isMaximumReached)
void setIsMinimumReached(boolean isMinimumReached)
void setItemId(int itemId)
void setScrollDirection(int scrollDirection)
Section 4 HGFastList library public methods and their descriptions.
void registerCallback(IHGFastList iHGFastList)
Function to register libraries main callback all gesture events are returned to this callback.

HGFastListInfo getHgFastList()
This is a convenience method in case you want to access the dynamic return type outside the
scope of the callback functions.

Drawable getForegroundDrawable()
Convenience method to access the libraries Canvas drawable.

void setScrollableView(final View scrollableView)
This library at present is only designed to work with RecycleList views. This is a key function that must be set in order for the library to work.
Note: For optimisation and open endedness, there is no code that relies on the ListView type so
refactoring the library to use a different list type is a simple refactoring job.

void setQuickTapTime(long quickTapTime)
Sets the quick tap time sensitivity in milliseconds.

void setAngleSnapBaseOne(final float angleSnapBaseOne, final float angleSnapProximity)
The library can have angle snapping behaviour on the dial that moves the list. The angles are
expressed in ranges of 0 � 1 instead of 0 to 360. For example if the first parameter is 0.125 then this
would cause the rotation to snap ever 45 degrees. If the second parameter was 0.03125 (1 quarter
of 0.125) then the dial would rotate freely for 22.5 degrees before a snap event occurs. Setting the
second parameter to one half of the first parameter would cause no free rotation between snap
events.

void setVariableDial(float variableDialInner, float variableDialOuter, float pagesPerDial)
The dial that moves the list up and down can rotate at variable speeds depending on how close the
touch is to the centre of the dial. The first two parameter values dictate how many times the dial
rotates for every rotation of the gesture. The final parameter dictates how many list pages scroll per
full circular gesture. Note: The speed of the dial will not affect the speed of the list unless you call: setUseVariablePages(boolean useVariablePages) with a value of true.
void setUseVariablePages(boolean useVariablePages) IMPORTANT! Key Feature
Calling with a value of true will cause the list to move at a speed relative to the dial rotation speed.
void setSuppressValidate(boolean suppressValidate)
When called with a value of false; simply prevents the dial from rotating, but list will still move in
the same way.
void setDialAngle(float fullGestureAngleBaseOne) (Not Yet Tested)
Sets the full angle of the dial. Note: this angle is directly related to how far the list has scrolled.
void performQuickTap()
Simple convenience method to programmatically execute a quick tap event.
long getQuickTapTime()
Simple getter to retrieve the milliseconds of the quick tap tolerance.
float getAngleSnapBaseOne() and float getAngleSnapProximity()
Simple getters to retrieve the values for the angle snapping behaviour.
OnTouchListener retrieveLocalOnTouchListener()
Convenience method to get a handle on the gesture listener for the library.
boolean hasAngleSnapped()
Retrieves the value for the angle snapping behaviour; also available from the callback
(HGFastListInfo type).
float getVariableDialInner(), float getVariableDialOuter() and float getItemsPerPage()
Simple getters, to get the inner/outer precision and items per page.
boolean getUseVariablePages()
Retrieves the vale set from setUseVariablePages(boolean useVariablePages)
float getPrecisionRotation()
Retrieves the variable sensitivity for how fast the dial is rotating in relation to how close the
gesture is to the centre of the dial; 1 representing one rotation per circular gesture.
boolean getSuppressValidate()
Simple getter to retrieve a state; the state being if the dial will move in response to the gesture.
float getDialAngle()
Retrieves the angle of the dial. Note this value is directly related to how far the list has moved.
View getScrollableView()
Simple getter to get the RecyclerListView. This returns a generic View type as an open end to make
is easier to refactor the library to use different ListView types.